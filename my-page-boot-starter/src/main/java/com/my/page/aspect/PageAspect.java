package com.my.page.aspect;

import com.my.page.commons.PageUtils;
import jakarta.servlet.http.HttpServletRequest;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@Aspect
public class PageAspect {

    /**
     * 所有标记了@RestController注解的类下的所有方法
     * @within  @annotation
     *
     * @return
     */
    @Around("@within(org.springframework.web.bind.annotation.RestController)")
    public Object pageHandler(ProceedingJoinPoint joinPoint){

        //1、获取请求参数
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        HttpServletRequest request = requestAttributes.getRequest();

        //2、获取相关参数
        String pageNumStr = request.getParameter("pageNum");
        String pageSizeStr = request.getParameter("pageSize");

        if (pageNumStr != null && pageSizeStr != null) {
            try {
                Integer pageNum = Integer.parseInt(pageNumStr);
                Integer pageSize = Integer.parseInt(pageSizeStr);
                //设置Page对象
                PageUtils.setPage(pageNum, pageSize);
            } catch (Exception e) {
            }
        }

        try {
            //直接执行后续的代码
            return joinPoint.proceed();
        } catch (Throwable e) {
            throw new RuntimeException(e);
        } finally {
            //ThreadLocal资源一定要回收，不然会内存泄漏
            PageUtils.clear();
        }
    }
}
