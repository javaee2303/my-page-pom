package com.my.page.config;

import com.my.page.aspect.PageAspect;
import com.my.page.plugin.MyPagePlugin;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * 自动配置类
 */
@Configuration
//启用AOP
@EnableAspectJAutoProxy
public class MyPageAutoConfiguration {

    /**
     * 注册AOP
     *
     *  @ConditionalOnProperty - 环境中有com.plugin.web属性，属性值为true时，当前的Bean才会被Spring加载
     *  @ConditionalOnBean - 判断当前的IOC容器中是否存在某个Bean
     *  @ConditionalOnClass - 判断当前的依赖中是否存在该类型的类
     * @return
     */
    @Bean
    @ConditionalOnProperty(prefix = "com.plugin", value = "web", havingValue = "true", matchIfMissing = true)
    public PageAspect pageAspect(){
        return new PageAspect();
    }

    /**
     * 注册分页插件
     * @return
     */
    @Bean

    public MyPagePlugin myPagePlugin(){
        return new MyPagePlugin();
    }
}
