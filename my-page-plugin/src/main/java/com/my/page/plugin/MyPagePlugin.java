package com.my.page.plugin;

import com.my.page.commons.PageUtils;
import com.my.page.entity.Page;
import org.apache.ibatis.executor.statement.StatementHandler;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.plugin.Intercepts;
import org.apache.ibatis.plugin.Invocation;
import org.apache.ibatis.plugin.Signature;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * 分页插件
 */
@Intercepts(@Signature(
        type = StatementHandler.class,
        method = "prepare",
        args = {Connection.class, Integer.class}
))
public class MyPagePlugin implements Interceptor {

    @Override
    public Object intercept(Invocation invocation) throws Throwable {

        //获取目标对象
        StatementHandler statementHandler = (StatementHandler) invocation.getTarget();
        //获取当前SQL语句
        BoundSql boundSql = statementHandler.getBoundSql();
        String sql = boundSql.getSql().toLowerCase().trim();

        if (!sql.startsWith("select")) {
            //不是查询语句，直接放行
            return invocation.proceed();
        }

        //判断是否要分页
        Page page = PageUtils.getPage();
        if (page == null) {
            //没有找到分页对象，则直接放行
            return invocation.proceed();
        }

        //去除末尾的分号
        if (sql.endsWith(";")) {
            sql = sql.substring(0, sql.length() - 1);
        }

        //查询总条数  select * from table where xxxfromxxx = ? group by xxxx order by
        //          select count(*) from table where xxxfromxxx = ? group by xxxx order by
        int pageCount = getAllCount(sql, invocation);
        //设置总条数
        page.setPageCount(pageCount);
        //设置总页码
        page.setPageSum(page.getPageCount() % page.getPageSize() == 0 ?
                page.getPageCount() / page.getPageSize() :
                page.getPageCount() / page.getPageSize() + 1);

        //边界处理
        if (page.getPageNum() > page.getPageSum()) page.setPageNum(page.getPageSum());
        if (page.getPageNum() < 1) page.setPageNum(1);

        //改造SQL
        sql += " limit " + (page.getPageNum() - 1) * page.getPageSize() + "," + page.getPageSize();
        System.out.println("分页SQL：" + sql);


        //想办法让改造后的SQL 设置到MyBatis里面去
        Class<BoundSql> boundSqlClass = BoundSql.class;
        Field field = boundSqlClass.getDeclaredField("sql");
        field.setAccessible(true);
        field.set(boundSql, sql);

        return invocation.proceed();
    }

    /**
     * 查询总条数
     * @return
     */
    private int getAllCount(String sql, Invocation invocation) {
        //获得目标对象
        StatementHandler statementHandler = (StatementHandler) invocation.getTarget();

        //构造总条数的sql
        int fromIndex = sql.indexOf(" from ");
        //获得总条数的SQL
        String countSql = "select count(*) as 'count' " + sql.substring(fromIndex);
        System.out.println("查询总条数的SQL：" + countSql);

        //获取拦截的目标对象的参数
        Object[] args = invocation.getArgs();
        Connection connections = (Connection) args[0];
        //直接用JDBC执行查询总条数的SQL
        ResultSet resultSet = null;
        try (
                PreparedStatement ps = connections.prepareStatement(countSql);
        ) {
            //设置参数，借助MyBatis本身设置SQL参数的方法，帮助我们设置参数
            statementHandler.parameterize(ps);
            //执行SQL
            resultSet = ps.executeQuery();
            if (resultSet.next()) {
                //获取总条数
                int count = resultSet.getInt("count");
                return count;
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            }
        }

        return 0;
    }
}
