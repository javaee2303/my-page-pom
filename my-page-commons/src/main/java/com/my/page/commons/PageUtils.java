package com.my.page.commons;

import com.my.page.entity.Page;

/**
 * 管理Page对象的工具方法
 */
public class PageUtils {

    private static ThreadLocal<Page> pageThreadLocal = new ThreadLocal<>();

    public static Page getPage() {
        return pageThreadLocal.get();
    }

    public static void setPage(Integer pageNum, Integer pageSize) {
        Page page = new Page()
                .setPageNum(pageNum)
                .setPageSize(pageSize);
        pageThreadLocal.set(page);
    }

    public static void clear(){
        pageThreadLocal.remove();
    }
}
