package com.my.page.entity;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 分页对象
 */
@Data
@Accessors(chain = true)
public class Page {
    private Integer pageNum;
    private Integer pageSize;
    private Integer pageCount;//总条数
    private Integer pageSum;//总页码
}
